#!/usr/bin/env python3
# encoding: utf-8

from Inventory import output, testInput
import pickle


with open(output,'rb') as fi:
    my_dict = pickle.load(fi)
index_word = my_dict['index_word']
word_index = my_dict['word_index']
with open(testInput,'rb') as f:
    my_dict = pickle.load(f)
word2idx = my_dict['word2idx']

countYes = 0
countNo = 0
for word in word_index:
    if word in word2idx:
        countYes += 1
       
    else:
        countNo += 1
        print(word)
print(countYes)
print(countNo)
f.close()
fi.close()

    

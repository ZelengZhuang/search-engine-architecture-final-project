#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json, pickle, time, collections
from sea_project.Inventory import trainning_input, test_input, output

news_dict = {}
word_index = {}
index_word = {}

time_start = time.clock()

word_index['<empty>'] = 0
index_word[0] = '<empty>'
word_index['<eos>'] = 1
index_word[1] = '<eos>'
count_num = 2

def process_text(text):
    text = text.replace('\t', ' '). replace('\n', ' ')
    text = list(text)
    for ind in range(len(text)):
        if  text[ind] == ' ':
            continue
        if text[ind] == '\'' :
            if ind < len(text) - 1 and  ind - 1 >= 0 and text[ind + 1].isalnum() and text[ind - 1].isalnum():
                continue
        if not text[ind].isalnum():
            text[ind] = ' '
    text = ''.join(str(e) for e in text)
    text = text.lower()
    return text
    
def mapWordIndex(text):
    terms = text.split()
    for term in terms:
        newTerm = term.strip()
        if not newTerm:
            continue
        if newTerm in word_index:
            word_index[newTerm] = word_index[newTerm] + 1
        else:
            word_index[newTerm] = 1

def buildIndex():
    index = len(word_index) + 1
    for key in word_index:
        word_index[key] = index
        index_word[index] = key
        index -= 1
        
with open(test_input, 'r') as myFile:
    for line in myFile:
        cur_news = json.loads(line)
        headlines = []
        contents = []
        if 'headline' in news_dict:
            headlines = news_dict['headline']
        if 'content'  in news_dict:
            contents = news_dict['content'] 
        
        title = str.lower(cur_news['title'])
        content = str.lower(cur_news['content'])
        content = content.split('\n')
        firtParagraph = ''
        count = 0
        for ind in range(len(content)):
            para = content[ind].strip()
            if para and len(para) > 10:
                firstParagraph = firtParagraph + para
                count += len(para)
            if count > 400:
                break
        title = process_text(title)
        firstParagraph = process_text(firstParagraph)
        contents.append(firstParagraph)
        headlines.append(title)
        mapWordIndex(title + ' ' + firstParagraph)
        news_dict['headline'] = headlines
        news_dict['content'] = contents
    word_index = collections.OrderedDict(sorted(word_index.items(), key=lambda x: x[1]))
    buildIndex()
myFile.close()
    
news_dict['word_index'] = word_index
news_dict['index_word'] = index_word

with open(output,'wb') as handle:
    pickle.dump(news_dict, handle)

time_end = time.clock()
print(time_end - time_start)

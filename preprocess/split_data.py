from argparse import ArgumentParser
import os, math
import random
from inventory import file_before_split

def main():
    parser = ArgumentParser()
    parser.add_argument("--data_path", dest = "data_path", required=True)
    args = parser.parse_args()
    data_path = args.data_path
    
    lines = []
    input = data_path + '/' + file_before_split
    with open(input, 'r') as myFile:
        for line in myFile:
            lines.append(line)
            
    trainning_file = open(str(data_path) + "/train.jsonl", 'w')
    test_file = open(str(data_path) + '/test.jsonl', 'w')
    test_train_file = open(str(data_path) + '/test_train.jsonl', 'w')
    
    for num in range(math.floor((len(lines) - 1) / 10)):
        start_index = num * 10
        end_index = (num + 1) * 10
        trainning_index = random.randint(start_index, end_index)
        left_indexes = list(range(start_index, trainning_index)) + list(range(trainning_index + 1, end_index))
        test_index = random.choice(left_indexes)
        trainning_file.write(lines[trainning_index])
        test_file.write(lines[test_index])
        test_train_file.write(lines[trainning_index])
        test_train_file.write(lines[test_index])
    trainning_file.close()
    test_file.close()
    test_train_file.close()
    

if __name__ == "__main__":
    main()
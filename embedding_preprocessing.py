import numpy as np
import helper

file_path = './glove.6B.300d.txt'
num_lines = sum(1 for line in open(file_path))


embeding_dimension = 300
NUM_PER_LINES_SHOWING_RESULTS = 10000
scale_factor = 0.1

word_to_index = {}
index_to_word = {}

cur_line = 0

embeding_weights = np.empty([num_lines, embeding_dimension])

with open(file_path, 'r') as f:
    for line in f:
        if cur_line % NUM_PER_LINES_SHOWING_RESULTS == 0:
            print('%f %% processed.' % (cur_line * 100 / num_lines))
        vals = line.strip().split()
        assert(embeding_dimension + 1 == len(vals))
        cur_word = vals[0].lower()
        word_to_index[cur_word] = cur_line
        index_to_word[cur_line] = cur_word
        embeding_weights[cur_line] = vals[1 : ]
        cur_line += 1

embeding_weights *= scale_factor

embeding_data = {}

embeding_data['weights'] = embeding_weights
embeding_data['word2idx'] = word_to_index
embeding_data['idx2word'] = index_to_word

helper.dump_data_to_file(embeding_data, 'embeding_data.p')








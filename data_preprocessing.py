import helper
import numpy as np
from sklearn.cross_validation import train_test_split


max_desc_length = 48
max_head_length = 24

data = helper.read_data_from_file('out.pickle')
embeding_data = helper.read_data_from_file('embeding_data.p')

vocab_size = 0
out_of_vocab_size = 0

for w, i in data['word_index'].items():
    if w in embeding_data['word2idx']:
        vocab_size += 1
    else:
        out_of_vocab_size += 1

final_data = {}

final_data['word2idx'] = {}
final_data['idx2word'] = {}

embedding = np.empty([vocab_size + 2, len(embeding_data['weights'][0])])

oov = []

cur = 2
for w in data['word_index']:
    if w in embeding_data['word2idx']:
        embedding[cur] = embeding_data['weights'][embeding_data['word2idx'][w]]
        final_data['word2idx'][w] = int(cur)
        final_data['idx2word'][cur] = w
        cur += 1
    else:
        oov.append(w)

for w in oov:
    final_data['word2idx'][w] = int(cur)
    final_data['idx2word'][cur] = w
    cur += 1

content = []
headline = []

for i in range(len(data['headline'])):
    c = np.empty(max_desc_length)
    h = np.empty(max_head_length)

    if i % 3000 == 0:
        print(i, len(data['headline']))

    for j in range(min(max_desc_length, len(data['content'][i].split()))):
        c[j] = int(final_data['word2idx'][data['content'][i].split()[j]])

    for j in range(min(max_head_length, len(data['headline'][i].split()))):
        h[j] = int(final_data['word2idx'][data['headline'][i].split()[j]])

    content.append(c)
    headline.append(h)

x_train, x_test, y_train, y_test = train_test_split(content, headline, test_size=0.2, random_state=77)

final_data['word2idx']['<empty>'] = 0
final_data['word2idx']['<eos>'] = 1
final_data['idx2word'][0] = '<empty>'
final_data['idx2word'][1] = '<eos>'

final_data['x_train'] = x_train
final_data['x_test'] = x_test
final_data['y_train'] = y_train
final_data['y_test'] = y_test

final_data['vocab_size'] = vocab_size + 2
final_data['out_of_vocab_size'] = out_of_vocab_size
final_data['embedding'] = embedding
final_data['embedding_dimension'] = len(embedding[0])

helper.dump_data_to_file(final_data, 'data.p')


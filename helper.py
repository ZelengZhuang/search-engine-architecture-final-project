import pickle

def dump_data_to_file(data, file):
    output = open(file, 'wb')
    pickle.dump(data, output)
    output.close()


def read_data_from_file(file):
    input = open(file, 'rb')
    data = pickle.load(input)
    input.close()
    return data


def take_first(dict, N):
    ans = {}
    for i in range(N):
        ans[i] = dict[i]
    return ans

def dis(v1, v2):
    assert(len(v1) == len(v2))
    result = 0
    for i in range(len(v1)):
        result += abs(v1[i] - v2[i])
    return result


def preprocess_and_save_data(dataset_path, token_lookup, create_lookup_tables):
    text = read_data_from_file(dataset_path)
    text = text[81:]

    token_dict = token_lookup()
    for key, token in token_dict.items():
        text = text.replace(key, ' {} '.format(token))

    text = text.lower()
    text = text.split()

    vocab_to_int, int_to_vocab = create_lookup_tables(text)
    int_text = [vocab_to_int[word] for word in text]
    pickle.dump((int_text, vocab_to_int, int_to_vocab, token_dict), open('preprocess.p', 'wb'))


def load_preprocess():
    return pickle.load(open('preprocess.p', mode='rb'))


def save_params(params):
    pickle.dump(params, open('params.p', 'wb'))


def load_params():
    return pickle.load(open('params.p', mode='rb'))
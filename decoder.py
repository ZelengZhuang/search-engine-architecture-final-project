import numpy as np, tensorflow as tf
import helper

data = helper.read_data_from_file('data.p')

def get_inputs():
    """
    Create TF Placeholders for input, targets, and learning rate.
    :return: Tuple (input, targets, learning rate)
    """
    inp = tf.placeholder(tf.int32, name='input', shape=(None, None))
    target = tf.placeholder(tf.int32, name='targets', shape=(None, None))
    learning_rate = tf.placeholder(tf.float32, name='learning_rate')
    return inp, target, learning_rate

def get_init_cell(batch_size, rnn_size):
    """
    Create an RNN Cell and initialize it.
    :param batch_size: Size of batches
    :param rnn_size: Size of RNNs
    :return: Tuple (cell, initialize state)
    """
    lstm_cell = tf.contrib.rnn.BasicLSTMCell(num_units=rnn_size)
    multiple_cells = tf.contrib.rnn.MultiRNNCell([lstm_cell])
    initial = multiple_cells.zero_state(batch_size, tf.float32)
    initial = tf.identity(initial, name='initial_state')
    return multiple_cells, initial


def get_embed(input_data, vocab_size, embed_dim):
    """
    Create embedding for <input_data>.
    :param input_data: TF placeholder for text input.
    :param vocab_size: Number of words in vocabulary.
    :param embed_dim: Number of embedding dimensions
    :return: Embedded input.
    """
    embedding = tf.Variable(tf.random_uniform((vocab_size, embed_dim), -1, 1))
    embed = tf.nn.embedding_lookup(embedding, input_data)
    return embed


def build_rnn(cell, inputs):
    """
    Create a RNN using a RNN Cell
    :param cell: RNN Cell
    :param inputs: Input text data
    :return: Tuple (Outputs, Final State)
    """
    output, final_state = tf.nn.dynamic_rnn(cell, inputs, dtype=tf.float32)
    final_state = tf.identity(final_state, 'final_state')
    return output, final_state


def build_nn(cell, embedding, input_data, vocab_size):
    """
    Build part of the neural network
    :param cell: RNN cell
    :param rnn_size: Size of rnns
    :param input_data: Input data
    :param vocab_size: Vocabulary size
    :return: Tuple (Logits, FinalState)
    """
    data_embeded = get_embed(input_data, vocab_size, rnn_size)
    output, final_state = build_rnn(cell, data_embeded)

    logits = tf.contrib.layers.fully_connected(
        output,
        vocab_size,
        weights_initializer=tf.truncated_normal_initializer(stddev=0.1),
        biases_initializer=tf.zeros_initializer(),
        activation_fn=None,
    )
    return logits, final_state


def get_batches(int_text, batch_size, seq_length):
    """
    Return batches of input and target
    :param int_text: Text with the words replaced by their ids
    :param batch_size: The size of batch
    :param seq_length: The length of sequence
    :return: Batches as a Numpy array
    """
    print(len(int_text), batch_size, seq_length)
    n_batches = int(len(int_text) / (batch_size * seq_length)) - 1

    total_num = batch_size * seq_length * n_batches

    x = np.array(int_text[: total_num])
    y = np.array(int_text[1: total_num + 1])

    x = np.split(x, batch_size * n_batches)
    x = np.split(np.array(x), n_batches)

    print(len(y), batch_size, n_batches)
    y = np.split(y, batch_size * n_batches)
    y = np.split(np.array(y), n_batches)

    ans = np.array(list(zip(x, y)))
    return ans

# Number of Epochs
num_epochs = 150
# Batch Size
batch_size = 256
# RNN Size
rnn_size = 256
# Sequence Length
seq_length = 24
# Learning Rate
learning_rate = 0.01
# Show stats for every n number of batches
show_every_n_batches = 2

save_dir = './save'

from tensorflow.contrib import seq2seq

train_graph = tf.Graph()
with train_graph.as_default():
    vocab_size = data['vocab_size']
    input_text, targets, lr = get_inputs()
    input_data_shape = tf.shape(input_text)
    cell, initial_state = get_init_cell(input_data_shape[0], rnn_size)
    logits, final_state = build_nn(cell, rnn_size, input_text, vocab_size)

    # Probabilities for generating words
    probs = tf.nn.softmax(logits, name='probs')

    # Loss function
    cost = seq2seq.sequence_loss(
        logits,
        targets,
        tf.ones([input_data_shape[0], input_data_shape[1]]))

    # Optimizer
    optimizer = tf.train.AdamOptimizer(lr)

    # Gradient Clipping
    gradients = optimizer.compute_gradients(cost)
    capped_gradients = [(tf.clip_by_value(grad, -1., 1.), var) for grad, var in gradients]
    train_op = optimizer.apply_gradients(capped_gradients)


y = data['y_train'][:5000]
batches = get_batches(np.array(y).flatten(), batch_size, seq_length)

with tf.Session(graph=train_graph) as sess:
    sess.run(tf.global_variables_initializer())

    for epoch_i in range(num_epochs):
        state = sess.run(initial_state, {input_text: batches[0][0]})

        for batch_i, (x, y) in enumerate(batches):
            feed = {
                input_text: x,
                targets: y,
                initial_state: state,
                lr: learning_rate}
            train_loss, state, _ = sess.run([cost, final_state, train_op], feed)

            # Show every <show_every_n_batches> batches
            if (epoch_i * len(batches) + batch_i) % show_every_n_batches == 0:
                print('Epoch {:>3} Batch {:>4}/{}   train_loss = {:.3f}'.format(
                    epoch_i,
                    batch_i,
                    len(batches),
                    train_loss))

    # Save Model
    saver = tf.train.Saver()
    saver.save(sess, save_dir)
    print('Model Trained and Saved')
import tensorflow as tf
import numpy as np
import helper
from nltk.corpus import stopwords

vocab = helper.read_data_from_file('vocab.p')
stopWords = set(stopwords.words('english'))

_, vocab_to_int, int_to_vocab, token_dict = helper.load_preprocess()
seq_length, load_dir = helper.load_params()


def get_tensors(loaded_graph):
    IT = loaded_graph.get_tensor_by_name("input:0")
    IST = loaded_graph.get_tensor_by_name("initial_state:0")
    FST = loaded_graph.get_tensor_by_name("final_state:0")
    PT = loaded_graph.get_tensor_by_name("probs:0")
    return (IT, IST, FST, PT)


def pick_word(probabilities, int_to_vocab):
    idx = np.argmax(probabilities)
    return int_to_vocab[idx]

def get_predict(seq):
    prime_word = "<error>"
    for s in seq:
        if s in stopWords:
            continue
        if s in vocab:
            prime_word = s
            break
    if prime_word == "<error>":
        return "Not enough infomation to generate"
    loaded_graph = tf.Graph()
    with tf.Session(graph=loaded_graph) as sess:
        loader = tf.train.import_meta_graph(load_dir + '.meta')
        loader.restore(sess, load_dir)

        input_text, initial_state, final_state, probs = get_tensors(loaded_graph)

        gen_sentences = [prime_word]
        prev_state = sess.run(initial_state, {input_text: np.array([[1]])})

        while True:
            dyn_input = [[vocab_to_int[word] for word in gen_sentences[-seq_length:]]]
            dyn_seq_length = len(dyn_input[0])

            probabilities, prev_state = sess.run(
                [probs, final_state],
                {input_text: dyn_input, initial_state: prev_state})

            pred_word = pick_word(probabilities[dyn_seq_length - 1], int_to_vocab)
            if pred_word == '||return||':
                break
            gen_sentences.append(pred_word)

        ans = ' '.join(gen_sentences)
        return ans
